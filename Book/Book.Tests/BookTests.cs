using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace Book.Tests
{
    [TestFixture]
    public class BookTests
    {
        private readonly string[] fields = { "datepublished", "totalpages", "published" };
        private readonly string[] props = { "title", "pages", "publisher", "isbn", "author", "price", "currency"};

        [Test]
        public void Book_Class_Is_Created()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            Assert.IsNotNull(bookType, "'Book' class is not created.");
        }

        [Test]
        public void All_Fields_Are_Defined()
        {
            var assemblyContent = LoadAssemblyContent();
            var notDefinedFields = new List<string>();

            var bookFields = GetAllNonPublicFields(assemblyContent);
            foreach (var field in this.fields)
            {
                
                var instanceField = bookFields.FirstOrDefault(f => f.Name.ToLowerInvariant().Contains(field, StringComparison.InvariantCulture));
                if (instanceField == null)
                {
                    notDefinedFields.Add(field);
                }
            }

            if (notDefinedFields.Count == 0)
            {
                notDefinedFields = null;
            }

            Assert.IsNull(notDefinedFields, $"Fields: {notDefinedFields?.Aggregate((previous, next) => $"'{previous}', {next}")} are not defined.");
        }

        [Test]
        public void All_Properties_Are_Defined()
        {
            var assemblyContent = LoadAssemblyContent();
            var notDefinedProperties = new List<string>();

            var bookFields = GetAllProperties(assemblyContent);
            foreach (var prop in this.props)
            {

                var instanceField = bookFields.FirstOrDefault(f => f.Name.ToLowerInvariant().Contains(prop, StringComparison.InvariantCulture));
                if (instanceField == null)
                {
                    notDefinedProperties.Add(prop);
                }
            }

            if (notDefinedProperties.Count == 0)
            {
                notDefinedProperties = null;
            }

            Assert.IsNull(notDefinedProperties, $"Fields: {notDefinedProperties?.Aggregate((previous, next) => $"'{previous}', {next}")} are not defined.");
        }

        private static FieldInfo[] GetAllNonPublicFields(Assembly assemblyContent)
        {
            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            return bookType?.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
        }

        private static PropertyInfo[] GetAllProperties(Assembly assemblyContent)
        {
            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            return bookType?.GetProperties(BindingFlags.Instance | BindingFlags.Public);
        }

        [Test]
        public void Parametrized_Constructor_Is_Defined()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            var constructor = bookType?.GetConstructors().FirstOrDefault(c =>
            {
                var parameters = c.GetParameters();
                if (parameters.Length > 0 &&
                    parameters.Count(p => p.ParameterType == typeof(string)) == 3 &&
                    parameters.Count(p => p.ParameterType == typeof(long)) == 1 
                    )
                {
                    return true;
                }

                return false;
            });

            Assert.IsNotNull(constructor, "Parametrized constructor is not defined.");
        }
        private static Assembly LoadAssemblyContent()
        {
            return Assembly.Load("Book");
        }

        [Test]
        public void Default_Constructor_Is_Defined()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));
            var defaultConstructor = bookType?.GetConstructor(Array.Empty<Type>());

            Assert.IsNotNull(defaultConstructor, "Default constructor is not defined.");
        }

        [Test]
        public void Publish_Method_Is_Defined()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            var method = bookType
                ?.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    if (m.ReturnType == typeof(void) && m.GetParameters().Length == 0 && m.Name == "Publish")
                    {
                        return true;
                    }

                    return false;
                });

            Assert.IsNotNull(method, "Publish method is not defined");
        }

        [Test]
        public void GetPublicationDate_Method_Is_Defined()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            var method = bookType
                ?.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    if (m.ReturnType == typeof(string) && m.GetParameters().Length == 0 && m.Name == "GetPublicationDate")
                    {
                        return true;
                    }

                    return false;
                });

            Assert.IsNotNull(method, "GetPublicationDate method is not defined");
        }


        [Test]
        public void SetPrice_Method_Is_Defined()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            var method = bookType?.GetMethods().FirstOrDefault(c =>
            {
                var parameters = c.GetParameters();
                if (parameters.Length == 2 && c.Name == "SetPrice" &&
                    parameters.Count(p => p.ParameterType == typeof(decimal)) == 1 &&
                    parameters.Count(p => p.ParameterType == typeof(string)) == 1)
                {
                    return true;
                }

                return false;
            });

            Assert.IsNotNull(method, "SetPrice is not defined.");
        }

        [Test]
        public void ToStringOverride_Method_Is_Defined()
        {
            var assemblyContent = LoadAssemblyContent();

            var bookType = assemblyContent.GetTypes()
                .FirstOrDefault(t => t.Name.Equals("book", StringComparison.OrdinalIgnoreCase));

            var method = bookType
                ?.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    if (m.ReturnType == typeof(string) && m.GetParameters().Length == 0 && m.Name == "ToString")
                    {
                        return true;
                    }

                    return false;
                });

            Assert.IsNotNull(method, "ToString method is not Overriden");
        }


    }
}
