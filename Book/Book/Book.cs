﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Book
{
    public sealed class Book
    {
        private bool published;
        private DateTime datePublished;
        private uint totalPages;

        public string Title { get; }
        public uint Pages { 
            get 
            {
                return totalPages;
            }
            set 
            { totalPages = value; } 
        }
        public string Publisher { get; }
        public long ISBN { get; }
        public string Author { get; }

        decimal _price;
        public decimal Price
        {
            get
            {
                return _price;
            }
            private set
            {
                Price = _price;
            }
        }

        string _currency;
        public string Currency
        {
            get
            {
                return _currency;
            }
            set
            {
                Currency = _currency;
            }
        }

        public Book()
        {
        }

        public Book(string author, string title, string publisher, long isbn)
        {
            Author = author;
            Title = title;
            Publisher = publisher;
            ISBN = isbn;
        }

        public Book(string author, string title, string publisher) :
            this(author, title, publisher, 98724132152)
        {
            Author = author;
            Publish();
        }

        public void Publish()
        {
            this.published = true;
            this.datePublished = DateTime.Now;
        }

        public string GetPublicationDate()
        {
            if (!published)
            {
                return "NYP";
            }
            else
            {
                return datePublished.ToString();
            }
        }

        public void SetPrice(decimal price, string currency)
        {
            _price = price;
            RegionInfo myRI1 = new RegionInfo($"{currency}");

            _currency = myRI1.ISOCurrencySymbol;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append(Title);
            sb.Append(" by ");
            sb.Append(Author);

            return sb.ToString();
        }

    }
}
